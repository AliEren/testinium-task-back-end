FROM golang:1.22.2

WORKDIR /app

COPY go.mod .
COPY . .

RUN go build -o bin .

EXPOSE 8080

ENTRYPOINT [ "/app/bin" ]