package test

import (
	"context"
	"log"
	"os"
	"testing"

	"github.com/joho/godotenv"
	"gitlab.com/AliEren/testinium-task-back-end/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func connectToTestDB() (*mongo.Client, *mongo.Collection) {
	if err := godotenv.Load(".env.test"); err != nil {
		log.Println("No .env file found")
	}

	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		log.Fatal("Set your 'MONGODB_URI' environment variable. " +
			"See: " +
			"www.mongodb.com/docs/drivers/go/current/usage-examples/#environment-variable")
	}

	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))
	if err != nil {
		panic(err)
	}

	db := client.Database("testiniumTest")
	coll := db.Collection("tasks")

	return client, coll
}

func disconnectDatabase(client *mongo.Client) {
	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()
}

func TestAddTask(t *testing.T) {
	client, coll := connectToTestDB()
	defer disconnectDatabase(client)

	taskID := database.AddTask(coll, "Hello world!")
	// In order to clear the state, delete the previously added task.
	result, err := coll.DeleteOne(context.TODO(), bson.M{"id": taskID})
	if err != nil {
		t.Fatalf("Error deleting the task with the ID of: %s", taskID)
	} else {
		t.Logf("Task is deleted, %v", result.DeletedCount)
	}
}

func TestGetAllTasks(t *testing.T) {
	client, coll := connectToTestDB()
	defer disconnectDatabase(client)

	var taskIDs []string
	length := 2

	for i := 0; i < length; i++ {
		taskIDs = append(taskIDs, database.AddTask(coll, "Hello world!"))
	}

	tasks, err := database.GetAllTasks(coll)
	if err != nil {
		t.Fatalf("Error occured while fetching all tasks.")
	}

	if len(tasks) != length {
		t.Fatalf("The length of the tasks does not match with the length value.\ntasks: %d, length: %d", len(tasks), length)
	}

	filter := bson.M{"id": &bson.M{"$in": taskIDs}}
	result, err := coll.DeleteMany(context.TODO(), filter)
	if err != nil {
		t.Fatalf("Error occured while deleting added tasks.")
	}

	t.Logf("Deleted documents count: %d", result.DeletedCount)
}

func TestDeleteTask(t *testing.T) {
	client, coll := connectToTestDB()
	defer disconnectDatabase(client)

	taskID := database.AddTask(coll, "Hello world!")
	task := database.Task{}
	err := coll.FindOne(context.TODO(), bson.M{"id": taskID}).Decode(&task)
	if err != nil {
		t.Fatal("Could not find added document.")
	}

	deletedDocumentsCount := database.DeleteTask(coll, task.ID)
	if deletedDocumentsCount != 1 {
		t.Fatal("Deleted document cound does not match with the added document count.")
	}
}

func TestUpdateTask(t *testing.T) {
	client, coll := connectToTestDB()
	defer disconnectDatabase(client)

	taskID := database.AddTask(coll, "Hello world!")
	task := database.Task{}
	err := coll.FindOne(context.TODO(), bson.M{"id": taskID}).Decode(&task)
	if err != nil {
		t.Fatal("Could not find added document.")
	}

	task.Checked = true
	updatedDocumentCount := database.UpdateTask(coll, task)
	if updatedDocumentCount != 1 {
		t.Fatal("Updated document count does not match with the added one.")
	}

	deletedDocumentCount := database.DeleteTask(coll, task.ID)
	if deletedDocumentCount != 1 {
		t.Fatal("Delete document count does not match with the added one.")
	}
}

func TestGetTaskIDByObjectID(t *testing.T) {
	client, coll := connectToTestDB()
	defer disconnectDatabase(client)

	taskID := database.AddTask(coll, "Hello world!")
	task := database.Task{}
	err := coll.FindOne(context.TODO(), bson.M{"id": taskID}).Decode(&task)
	if err != nil {
		t.Fatal("Could not find added document.")
	}

	result, err := coll.DeleteOne(context.TODO(), bson.M{"id": taskID})
	if err != nil {
		t.Fatalf("Error deleting the task with the ID of: %s", taskID)
	} else {
		t.Logf("Task is deleted, %v", result.DeletedCount)
	}
}
