package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/AliEren/testinium-task-back-end/database"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	// Database connection.
	if err := godotenv.Load(); err != nil {
		log.Println("No .env file found")
	}

	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		log.Fatal("Set your 'MONGODB_URI' environment variable. " +
			"See: " +
			"www.mongodb.com/docs/drivers/go/current/usage-examples/#environment-variable")
	}

	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))
	if err != nil {
		panic(err)
	}

	defer func() {
		if err := client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	db := client.Database("testinium")
	coll := db.Collection("tasks")

	server(coll)
}

func server(coll *mongo.Collection) {
	router := gin.Default()
	setCORSPolicy(router)
	apiRequests(router, coll)

	// Listening on Port: localhost:8080
	router.Run(":8080")
}

func setCORSPolicy(router *gin.Engine) {
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:3000"}
	config.AllowMethods = []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"}
	config.AllowHeaders = []string{"Origin", "Content-Type", "Authorization", "Accept"}
	router.Use(cors.New(config))
}

func apiRequests(router *gin.Engine, coll *mongo.Collection) {
	router.GET("/get-all-tasks", func(c *gin.Context) {
		taskList, err := database.GetAllTasks(coll)
		if err != nil {
			panic(err)
		}

		c.JSON(200, gin.H{
			"response": taskList,
		})
	})

	router.POST("/add-task", func(c *gin.Context) {
		var task database.TaskValue
		err := c.BindJSON(&task)
		if err != nil {
			fmt.Println("Error occured while BindJSON to TaskValue struct.")
		}

		taskID := database.AddTask(coll, task.Value)
		c.JSON(200, gin.H{
			"response": taskID,
		})
	})

	router.POST("/delete-task", func(c *gin.Context) {
		var taskID database.TaskID
		err := c.BindJSON(&taskID)
		if err != nil {
			fmt.Println("Error occured while BindJSON to TaskID struct.")
		}

		// Delete a task.
		deletedDocumentCount := database.DeleteTask(coll, taskID.ID)
		c.JSON(200, gin.H{
			"response": deletedDocumentCount,
		})
	})

	router.POST("/update-task", func(c *gin.Context) {
		var task database.Task
		err := c.BindJSON(&task)
		if err != nil {
			fmt.Println("Error occured while BindJSON to Task struct.")
		}

		// Update Task
		updatedTaskCount := database.UpdateTask(coll, task)
		c.JSON(200, gin.H{
			"response": updatedTaskCount,
		})
	})
}
