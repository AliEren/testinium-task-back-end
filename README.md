# Testinium To-Do App Front-End
+ https://gitlab.com/AliEren/testinium-task-back-end

## Description
This project is created with GoLang and MongoDB for keeping the database. The project provides bunch of APIs to perform CRUD operations on the To-Do tasks. Since I could not manage the deployment of the back-end through CI/CD, the project runs in local environment only. 

## Structure
The program starts in "main.go" file. In main function in main.go file I connect to the database with using my MongoDB credentials. After establishing connection, started the localhost in port:8080. Also I set CORS policy for incoming front-end requests.

As a first step program accepts incoming calls and acccording to the APIs, calls the requested API's function from the "./database/database.go". As the name suggests this is the file that all of the database operations are performed.

In "./helpers/helpers.go" file any extra helper functions resides. Also "./test/database_test.go" file contains the codes that perform tests for the back-end application.

## Tests
For the test environment I created a new database in MongoDB and run tests by using GoLang's default test library called "testing". In "database_test.go" file, I test every function inside the "database.go".

## Pipeline
Because of I could not deploy the back-end project, my pipeline looks pretty simple. I just have two stages for this section. At the beginning of the pipeline I put the docker image so that subsequest stages can use the image.
### 1. Build Stage
+ script: In order to get the binaries of the project I installed the dependencies and buld the project.
+ artifacts: The built is saved to GitLab artifacts for future operations.
### 2. Test Stage
+ script: With this script I run the test folder.

## Dockerfile
In order to create a Docker image I used "golang:1.22.2" image. As the project directory I specified "/app" directory. After specifying directory, I put binaries into this folder. As my last steps I assign port number 8080 for the image and set the application directory.

## Database
As a database I go with MongoDB. There are two databases overal. The one is for prod and the other one is for testing purposes. The prod database is named as "testinium" and the test database as "testiniumTest". Both of these databases has one collection called "tasks".

## Libraries Used In This Project
+ Google UUID
+ Mongo Driver
+ Gin Gonic
+ godotenv
+ testing