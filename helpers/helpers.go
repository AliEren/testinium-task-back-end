package helpers

import (
	"strings"

	"github.com/google/uuid"
)

func GenerateUUID() string {
	uuidRaw, err := uuid.NewRandom()
	if err != nil {
		panic(err)
	}

	uuidStr := uuidRaw.String()
	uuidStr = strings.ReplaceAll(uuidStr, "-", "")
	uuidStr = uuidStr[:16]

	return uuidStr
}
