package database

import (
	"context"
	"fmt"

	"gitlab.com/AliEren/testinium-task-back-end/helpers"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type TaskValue struct {
	Value string `json:"value" bson:"value"`
}

type TaskID struct {
	ID string `json:"id" bson:"id"`
}

type Task struct {
	ID      string `json:"id" bson:"id"`
	Value   string `json:"value" bson:"value"`
	Checked bool   `json:"checked" bson:"checked"`
}

func AddTask(coll *mongo.Collection, text string) string {
	task := Task{
		ID:      helpers.GenerateUUID(),
		Value:   text,
		Checked: false,
	}

	taskDoc, err := bson.Marshal(task)
	if err != nil {
		panic(err)
	}

	addedTask, err := coll.InsertOne(context.TODO(), taskDoc)
	if err != nil {
		panic(err)
	}

	taskObjectID := addedTask.InsertedID.(primitive.ObjectID)
	taskID := getTaskIDByObjectID(coll, taskObjectID)
	return taskID
}

func GetAllTasks(coll *mongo.Collection) ([]Task, error) {
	var tasks []Task
	filter := bson.D{}

	cursor, err := coll.Find(context.TODO(), filter)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.TODO())

	for cursor.Next(context.Background()) {
		var task Task

		err := cursor.Decode(&task)
		if err != nil {
			return nil, err
		}

		tasks = append(tasks, task)
	}

	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return tasks, nil
}

func DeleteTask(coll *mongo.Collection, id string) int64 {
	result, err := coll.DeleteOne(context.TODO(), bson.M{"id": id})
	if err != nil {
		panic(err)
	}

	return result.DeletedCount
}

func UpdateTask(coll *mongo.Collection, task Task) int64 {
	result, err := coll.UpdateOne(context.TODO(), bson.M{"id": task.ID}, bson.M{"$set": task})
	if err != nil {
		panic(err)
	}

	return result.ModifiedCount
}

func getTaskIDByObjectID(coll *mongo.Collection, objectId primitive.ObjectID) string {
	var taskID TaskID

	err := coll.FindOne(context.TODO(), bson.M{"_id": objectId}).Decode(&taskID)
	if err != nil {
		fmt.Println("Error occured in getTaskIDByObjectID function: \n", err)
	}

	return taskID.ID
}
